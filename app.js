/*  1 - Install python 
        * sudo add-apt-repository ppa:deadsnakes/ppa
        * sudo apt-get update
        * sudo apt-get install python2.7
    2 - Invoke a simple http server
        * python -m SimpleHTTPServer
    3 - Example of a post request
        body: {"properties": {"eq": [["PROCE_RE", "303170140"]]}}
*/

/* MAP */
var map;

/* Ids of the divs */
var idProcedure = ['CNES', 'CMPT', 'ESPECIALID', 'CAR_INTEN', 'DIAG_PR', 'DIAG_SE1', 
        'DIAG_SE2', 'COMPLEXIDA', 'FINACIAME'];
var idPatientInfo = ['age_group', 'P_RACA', 'LV_INSTRU'];
var idEstablishment = ['DA', 'PR', 'STS', 'CRS', 'GESTOR_IDE'];

var cluster = null, clean_up_cluster = null;

new Vue({
    el: '#vue-app',
    data: {
        procedure: [
            {title: 'Estabelecimento do atendimento prestado.', display: 'Estabelecimento de ocorrência', id: 'CNES'},
            {title: 'Ano/mês de processamento da AIH. Ex.: 201506 (junho de 2015)', display: 'Competência (aaamm)', id: 'CMPT'},
            {title: 'Especialidade do leito de internação.', display: 'Especialidade do leito', id: 'ESPECIALID'},
            {title: 'Caráter da internação.', display: 'Caráter do atendimento', id: 'CAR_INTEN'},
            {title: 'Motivo da internação.', display: 'Diagnóstico principal (CID-10)', id: 'DIAG_PR'},
            {title: 'Motivo que levou ao diagnóstico principal.', display: 'Diagnóstico secundário (CID-10)', id: 'DIAG_SE1'},
            {title: 'Motivo que levou ao diagnóstico principal.', display: 'Diagnóstico secundário 2 (CID-10)', id: 'DIAG_SE2'},
            {title: 'Nível de atenção para a realização do procedimento.', display: 'Complexidade', id: 'COMPLEXIDA'},
            {title: 'Tipo de financiamento da internação.', display: 'Tipo de financiamento', id: 'FINACIAME'},
        ],
        patient_info: [
            {title: 'Faixa etária do paciente.', display: 'Faixa etária', id: 'age_group'},
            {title: 'Raça/Cor do paciente.', display: 'Raça/Cor', id: 'P_RACA'},
            {title: 'Grau de instrução do paciente.', display: 'Grau de instrução do paciente', id: 'LV_INSTRU'},
        ],
        establishment: [
            {title: 'Distrito administrativo da internação.', display: 'Distrito administrativo', id: 'DA'},
            {title: 'Subprefeitura do estabelecimento.', display: 'Subprefeitura', id: 'PR'},
            {title: 'Supervisão técnica de saúde.', display: 'Supervisão Técnica de Saúde', id: 'STS'},
            {title: 'Coordenadoria regional de saúde.', display: 'Coordenadoria Regional de Saúde', id: 'CRS'},
            {title: 'Secretaria responsável.', display: 'Gestão', id: 'GESTOR_IDE'},
        ],
        optionsProcedure: openOptionsProcedure(),
        optionsPatientInfo: openOptionsPatientInfo(),
        optionsEstablishment: openOptionsEstablishment(),
    },
});

function init(){
    initProcedureMap();
}

/* Open the json files to fill the options */
function openOptionsProcedure(){
    //var pathFilesProcedure = [healthCentres, cmpt, specialties, treatments, cid, cid, cid, complexity, finance];
    //var pathFiles
    var pathFilesProcedure = ["health_centres.json", "cmpt.json", "specialties.json", "treatments.json", "CID10.json", "CID10.json", "CID10.json", "complexity.json", "finance.json"];
    var optionsProcedure = [];
    /* synchronous calls */
    $.ajaxSetup({
        async: false
    });
    for(index in pathFilesProcedure){
        $.getJSON("json_files/" + pathFilesProcedure[index], function(json) {
            optionsProcedure.push(json);
        }); 
    }
    return optionsProcedure;
}
function openOptionsPatientInfo(){
    var pathFilesPatientInfo = ["age_group.json", "race.json", "lv_instruction.json"];
    optionsPatientInfo = [];
    for(index in pathFilesPatientInfo){
        $.getJSON("json_files/" + pathFilesPatientInfo[index], function(json) {
            optionsPatientInfo.push(json);
        }); 
    }
    return optionsPatientInfo;
}
function openOptionsEstablishment(){
    var pathFilesEstablishment = ["DA.json", "PR.json", "STS.json", "CRS.json", "gestor.json"];
    optionsEstablishment = [];
    for(index in pathFilesEstablishment){
        $.getJSON("json_files/" + pathFilesEstablishment[index], function(json) {
            optionsEstablishment.push(json);
        }); 
    }
    return optionsEstablishment;
}

/* Initialize the map */
function initProcedureMap(){
    clean_up_cluster = []

    var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 18,
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }),
    latlng = L.latLng(-23.557296000000001, -46.669210999999997);

    map = L.map('procedure_map', { center: latlng, zoom: 11, layers: [tiles] });
    map.on('contextmenu',function(e){
        popup.setLatLng(e.latlng);
        map.openPopup(popup);
    });
}

function cleanFilters(){
    for (index in idProcedure){
        document.getElementById(idProcedure[index]).value = '';
    }
    for (index in idPatientInfo){
        document.getElementById(idPatientInfo[index]).value = '';
    }
    for (index in idEstablishment){
        document.getElementById(idEstablishment[index]).value = '';
    }
    mapCleanUp();
}

function mapCleanUp() {
    if (cluster != null)
        map.removeLayer(cluster);
}

function getData(){
    var result = [];
    for (index in idProcedure){
        var data = document.getElementById(idProcedure[index]).value;
        if(typeof data !== "undefined" && data !== null && data !== ''){
            result[idProcedure[index]] = data;
        }
        else{
            result[idProcedure[index]] = '';
        }
    }
    
    for (index in idPatientInfo){
        var data = document.getElementById(idPatientInfo[index]).value;
        if(typeof data !== "undefined" && data !== null && data !== ''){
            result[idPatientInfo[index]] = data;
        }
        else{
            result[idPatientInfo[index]] = '';
        }
    }
    
    for (index in idEstablishment){
        var data = document.getElementById(idEstablishment[index]).value;
        if(typeof data !== "undefined" && data !== null && data !== ''){
            result[idEstablishment[index]] = data;
        }
        else{
            result[idEstablishment[index]] = '';
        }
    }
    return result;
}

function search() {
    mapCleanUp();
    data = getData();
    parsedData = parseData(data);
    parsedData["group"] = {"count": ["coordinates"]};
    cluster = L.markerClusterGroup({
        maxClusterRadius: 80,
        chunkedLoading: true,
        iconCreateFunction: createCustomCluster
    });

    path = "http://localhost:3000/api/group";
    $.ajax({
        type: 'post',
        url: path,
        data: {data: JSON.stringify(parsedData)},
        crossDomain: true,
        dataType: 'json',
    }).done(populateCluster);
}

function createCustomCluster(cluster) {
    var markers = cluster.getAllChildMarkers();
    var totalCluster = 0;
    for (var i = 0; i < markers.length; i++) {
        totalCluster += markers[i].number;
    }

    if (totalCluster < 5000) {
        className = 'map-marker marker-5k a-class';
        size = 44;
    } else if (totalCluster < 100000) {
        className = 'map-marker marker-10k a-class';
        size = 64;
    } else if (totalCluster >= 100000) {
        className = 'map-marker marker-100k a-class';
        size = 84;
    }
    return L.divIcon({ html: totalCluster, className: className, iconSize: L.point(size, size) });
}

function populateCluster(featureCollection) {
    var markerList = []

    $.each(featureCollection["features"], function (index, feature) {
        counter = feature["properties"]["counter"];
        coordinates = feature["geometry"]["coordinates"];
        icon = L.divIcon({ html: counter, className: 'map-marker marker-single a-class', iconSize: L.point(34, 34) });
        marker = L.marker(L.latLng(coordinates[1], coordinates[0]), {icon: icon, title: counter + ' internações no mesmo setor censitário'})
        marker.latlong = [coordinates[0], coordinates[1]];
        marker.number = counter;
        marker.clusterOpen = false;
        marker.cluster = null;
        marker.id = counter;
        marker.on('click', CustomMarkerOnClick);
        markerList.push(marker);
    });

    cluster.addLayers(markerList);
    map.addLayer(cluster);
}

// TODO
function CustomMarkerOnClick(e) {
    marker = e.target
    if (marker.cluster == null) { //Happens only once per cluster
        marker.cluster = L.markerClusterGroup({
            chunkedLoading: true,
            iconCreateFunction: function(cluster) {
                return L.divIcon({ html: "", className: "Invisible Cluster", iconSize: L.point(0, 0) });
            }
        });
        var dotIcon = L.icon({
            iconUrl: "https://storage.googleapis.com/support-kms-prod/SNP_2752125_en_v0", iconAnchor: [5, 0]
        });

        path = "http://localhost:3000/api/query";

        list = []
        data = getData()
        parsedData = parseData(data)
        parsedData["properties"]["location"] = marker.latlong

        $.ajax({
            type: 'post',
            url: path,
            data: {data: JSON.stringify(parsedData)},
            crossDomain: true,
            dataType: 'json'
        }).done(function(data){
            $.each(data["features"], function(index, id){
                single_point_marker = L.marker(L.latLng(marker.latlong[1], marker.latlong[0]), {icon: dotIcon, id: id}).on('click', markerOnClick);
                single_point_marker.properties = id["properties"]
                list.push(single_point_marker)
            });
            marker.cluster.addLayers(list);
            marker.cluster.addTo(map);
            clean_up_cluster.push(marker.cluster);

            $.each(marker.cluster.getLayers(), function(index, layer) {
                layer.__parent.spiderfy();
            });
            marker.clusterOpen = true;
            // console.log(data)
        });
    }

    if (!marker.clusterOpen) {
        $.each(marker.cluster.getLayers(), function(index, layer) {
            layer.__parent.spiderfy();
        });
        marker.clusterOpen = true;
    } else {
        $.each(marker.cluster.getLayers(), function(index, layer) {
            layer.__parent.unspiderfy();
        });
        marker.clusterOpen = false;
    }
}

function markerOnClick(e) {
    // console.log(e)
    // console.log(e.target.properties)
    text = ""
    $.each(e.target.properties, function(index, value) {
        text += "<strong>" + index + ": </strong>" + value + "<br>";
    })
    // text = JSON.stringify(e.target.properties)
    e.target.bindPopup(text, {direction:'top'});
    e.target.openPopup();

}

function parseData(data) {
    parsedData = {};
    parsedData["properties"] = {}
    parsedData["properties"]["eq"] = []
    idProcedure.forEach(build);
    idPatientInfo.forEach(build);
    idEstablishment.forEach(build);

    return parsedData
}

function build(currentValue, index, arr) {
    if (data[currentValue] !== "") 
        parsedData["properties"]["eq"].push([currentValue, data[currentValue]]);
}
